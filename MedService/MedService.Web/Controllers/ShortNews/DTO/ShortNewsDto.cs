﻿using MedService.Web.Controllers.Comments.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace MedService.Web.Controllers.Snapshots.DTO
{
  public class ShortNewsDto
  {
    public string Id { set; get; }
    public string Header { set; get; }
    public string Body { set; get; }
    public DateTime CreateDate { set; get; }

    public IEnumerable<CommentDto>? Comments { set; get; }
  }
}
