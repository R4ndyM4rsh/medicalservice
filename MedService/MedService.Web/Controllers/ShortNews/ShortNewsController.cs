﻿
using MedService.Core.Domains.ShortsNews.Services;
using MedService.Core.Domains.ShortsNews;
using MedService.Web.Controllers.Comments.DTO;
using MedService.Web.Controllers.Snapshots.DTO;
using MedService.Web.Controllers.Users.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedService.Web.Controllers.Snapshots
{
  [ApiController]
  [Route("[controller]")]
  public class ShortNewsController
  {
    private readonly IShortNewsService _shortNewsService;

    public ShortNewsController(IShortNewsService shortNewsService) 
    {
      _shortNewsService = shortNewsService;
    }

    [HttpPost("CreateShortNews")]
    public async Task CreateShortNews(string header, string body) 
    {
      await _shortNewsService.CreateShortNews(header, body);
    }

    [HttpGet("GetShortNewsById")]
    public async Task<ShortNewsDto> GetShortNewsById(string id) 
    {
      var shortNews = await _shortNewsService.GetShortNewsById(id);
      return new ShortNewsDto
      {
        Id = shortNews.Id,
        Header = shortNews.Header,
        Body = shortNews.Body,
        CreateDate = shortNews.CreateDate,
        Comments = shortNews.Comments.Select(co => new CommentDto
        {
          Id = co.Id,
          ShortNewsId = shortNews.Id,
          Text = co.Text,
          TimeCreate = co.TimeCreate,
          isUpdate = co.isUpdate,
          User = new UserDto
          {
            Id = co.User.Id,
            Login = co.User.Login,
            Avatar = co.User.Avatar,
            Email = co.User.Email,
            Name = co.User.Name
          }
        })
      };
    }

    [HttpGet("GetAllShortNews")]
    public async Task<IEnumerable<ShortNewsDto>> GetAllShortNews() 
    {
      var shortsNews = await _shortNewsService.GetAllShortsNews();
      return shortsNews.Select(it => new ShortNewsDto
      {
        Id = it.Id,
        Header = it.Header,
        Body = it.Body,
        CreateDate = it.CreateDate,
        Comments = it.Comments.Select(co => new CommentDto
        {
          Id = co.Id,
          ShortNewsId = it.Id,
          Text = co.Text,
          TimeCreate = co.TimeCreate,
          isUpdate = co.isUpdate,
          User = new UserDto
          {
            Id = co.User.Id,
            Login = co.User.Login,
            Avatar = co.User.Avatar,
            Email = co.User.Email,
            Name = co.User.Name
          }
        })
      });
    }

    [HttpPut("UpdateShortNews/{id}")]
    public async Task Update(string id, string header, string body)
    {
      await _shortNewsService.Update(id, header, body);
    }
  }
}

