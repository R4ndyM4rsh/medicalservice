﻿using MedService.Core.Domains.Parkensons.Services;
using MedService.Web.Controllers.Parkensons.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedService.Web.Controllers.Parkensons
{
  [ApiController]
  [Route("[controller]")]
  public class ParkensonController
  {
    private readonly IParkensonService _parkensonService;

    public ParkensonController(IParkensonService parkensonService)
    {
      _parkensonService = parkensonService;
    }

    [HttpPost("CreateAnalysis")]
    public async Task Create(ParkensonModelDtoCreate model)
    {
      await _parkensonService.Create(model.userId, model.leftAtt, model.rightAtt, model.leftMot, model.rightMot);
    }

    [HttpDelete("DeleteAnalysis")]
    public async Task Delete(string id)
    {
      await _parkensonService.Delete(id);
    }

    [HttpGet("GetAll")]
    public async Task<IEnumerable<ParkensonModelDto>> GetAll()
    {
      var parkenson = await _parkensonService.GetAll();
      return parkenson.Select(it => new ParkensonModelDto
      {
        Id = it.Id,
        userId = it.userId,
        leftAtt = it.leftAtt,
        rightAtt = it.rightAtt,
        leftMot = it.leftMot,
        rightMot = it.rightMot,
        result = it.result
      });
    }

    [HttpGet("GetById")]
    public async Task<ParkensonModelDto> GetById(string id)
    {
      var parkenson = await _parkensonService.GetById(id);
      return new ParkensonModelDto
      {
        Id = parkenson.Id,
        userId = parkenson.userId,
        leftAtt = parkenson.leftAtt,
        rightAtt = parkenson.rightAtt,
        leftMot = parkenson.leftMot,
        rightMot = parkenson.rightMot,
        result = parkenson.result
      };
    }

    [HttpGet("GetByUserId")]
    public async Task<IEnumerable<ParkensonModelDto>> GetByUserId(string userId)
    {
      var parkenson = await _parkensonService.GetByUserId(userId);
      return parkenson
        .Where(it => it.userId == userId)
        .Select(it => new ParkensonModelDto
        {
          Id = it.Id,
          userId = it.userId,
          leftAtt = it.leftAtt,
          rightAtt = it.rightAtt,
          leftMot = it.leftMot,
          rightMot = it.rightMot,
          result = it.result
        });
    }
  }
}
