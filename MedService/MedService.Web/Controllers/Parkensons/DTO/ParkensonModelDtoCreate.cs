﻿namespace MedService.Web.Controllers.Parkensons.DTO
{
  public class ParkensonModelDtoCreate
  {
    public string userId { set; get; }
    public double[][] leftAtt { set; get; }
    public double[][] rightAtt { set; get; }
    public double[][] leftMot { set; get; }
    public double[][] rightMot { set; get; }
  }
}
