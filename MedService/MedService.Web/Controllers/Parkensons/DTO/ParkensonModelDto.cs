﻿namespace MedService.Web.Controllers.Parkensons.DTO
{
  public class ParkensonModelDto
  {
    public string Id { set; get; }
    public string userId { set; get; }
    public double[][] leftAtt { set; get; }
    public double[][] rightAtt { set; get; }
    public double[][] leftMot { set; get; }
    public double[][] rightMot { set; get; }
    public string result { set; get; }
  }
}
