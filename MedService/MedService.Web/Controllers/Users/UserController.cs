﻿using MedService.Core.Domains.Users.Services;
using MedService.Web.Controllers.Users.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedService.Web.Controllers.Users
{
  [ApiController]
  [Route("[controller]")]
  public class UserController
  {
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
      _userService = userService;
    }

    [HttpGet("GetAllUser")]
    public async Task<IEnumerable<UserDto>> GetAll()
    {
      var users = await _userService.GetAll();
      return users.Select(it => new UserDto
      {
        Id = it.Id,
        Login = it.Login,
        Email = it.Email,
        Name = it.Name,
        Avatar = it.Avatar
      });
    }

    [HttpGet("GetUserById/{id}")]
    public async Task<UserDto> GetById(string id)
    {
      var model = await _userService.GetUserById(id);
      return new UserDto
      {
        Id = model.Id,
        Login = model.Login,
        Email = model.Email,
        Name = model.Name,
        Avatar = model.Avatar
      };
    }

    [HttpPost("CreateUser")]
    public async Task Create(CreateUserDto model)
    {
      await _userService.Create(model.Login, model.Email, model.Name, model.File);
    }

    [HttpPut("UpdateUser/{id}")]
    public async Task Update(string id, string login, string email, string name)
    {
      await _userService.Update(id, login, email, name);
    }

    [HttpDelete("DeleteUser/{id}")]
    public async Task Delete(string id)
    {
      await _userService.Delete(id);
    }
  }
}
