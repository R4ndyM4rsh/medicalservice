﻿using Microsoft.AspNetCore.Http;

namespace MedService.Web.Controllers.Users.Dto
{
  public class UserDto
  {
    public string Id { set; get; }
    public string Login { set; get; }
    public byte[] Avatar { set; get; }
    public string Email { set; get; }
    public string Name { set; get; }
  }
}
