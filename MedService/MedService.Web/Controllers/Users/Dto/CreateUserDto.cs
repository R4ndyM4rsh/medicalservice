﻿using Microsoft.AspNetCore.Http;

namespace MedService.Web.Controllers.Users.Dto
{
  public class CreateUserDto
  {
    public string Login { set; get; }
    public IFormFile File { set; get; }
    public string Email { set; get; }
    public string Name { set; get; }
  }
}
