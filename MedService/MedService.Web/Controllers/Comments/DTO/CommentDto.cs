﻿using MedService.Web.Controllers.Users.Dto;
using System;

namespace MedService.Web.Controllers.Comments.DTO
{
  public class CommentDto
  {
    public string Id { set; get; }

    public string ShortNewsId { set; get; }

    public UserDto User { set; get; }

    public string Text { set; get; }

    public bool isUpdate { set; get; }
    public DateTime TimeCreate { set; get; }
  }
}
