﻿using MedService.Core.Domains.Comments.Services;
using MedService.Core.Domains.Comments;
using MedService.Core.Domains.Users;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MedService.Web.Controllers.Comments.DTO;
using MedService.Web.Controllers.Users.Dto;
using System.Collections.Generic;
using System.Linq;

namespace MedService.Web.Controllers.Comments
{
  [ApiController]
  [Route("[controller]")]
  public class CommentController
  {
    private readonly ICommentService _commentService;

    public CommentController(ICommentService commentService) 
    {
      _commentService = commentService;
    }
    [HttpPost("CreateComment")]
    public async Task CreateComment(string newsId, string text, User user)
    {
      await _commentService.CreateComment(newsId, text, user);
    }

    [HttpDelete("DeleteComment")]
    public async Task DeleteComment(string id) 
    {
      await _commentService.DeleteComment(id);
    }

    [HttpPut("UpdateComment")]
    public async Task UpdateComment(string id, string text) 
    {
      await _commentService.UpdateComment(id,text);
    }

    [HttpGet("GetCommentById")]
    public async Task<CommentDto> GetCommentById(string id) 
    {
      var comment = await _commentService.GetCommentById(id);
      return new CommentDto
      {
        Id = comment.Id,
        ShortNewsId = comment.ShortNewsId,
        User = new UserDto 
        {
          Id = comment.User.Id,
          Login = comment.User.Login,
          Avatar = comment.User.Avatar,
          Email = comment.User.Email,
          Name = comment.User.Name
        },
        Text = comment.Text,
        isUpdate = comment.isUpdate,
        TimeCreate = comment.TimeCreate
      };
    }

    [HttpGet("GetAllComments")]
    public async Task<IEnumerable<CommentDto>> GetAllComment() 
    {
      var comments = await _commentService.GetAllComments();
      return comments.Select(it => new CommentDto 
      {
        Id = it.Id,
        ShortNewsId = it.ShortNewsId,
        Text = it.Text,
        TimeCreate = it.TimeCreate,
        isUpdate = it.isUpdate,
        User = new UserDto 
        {
          Id = it.User.Id,
          Login = it.User.Login,
          Email = it.User.Email,
          Name = it.User.Name,
          Avatar = it.User.Avatar
        }
      });
    }
  }
}
