﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace MedService.Web.Middlewares
{
  public class ExceptionMiddleware
  {
    public readonly RequestDelegate next;

    public ExceptionMiddleware(RequestDelegate next)
    {
      this.next = next;
    }

    public async Task Invoke(HttpContext httpContext)
    {
      try
      {
        await next(httpContext);
      }
      catch (Exception exception)
      {
        httpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
        await httpContext.Response.WriteAsJsonAsync(new { exception.Message });
      }
    }
  }

}
