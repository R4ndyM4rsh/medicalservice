﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace MedService.Web.Middlewares
{
  public class ValidationExceptionMiddleware
  {
    public readonly RequestDelegate next;

    public ValidationExceptionMiddleware(RequestDelegate next)
    {
      this.next = next;
    }

    public async Task Invoke(HttpContext httpContext)
    {
      try
      {
        await next(httpContext);
      }
      catch (ValidationException exception)
      {
        httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        await httpContext.Response.WriteAsJsonAsync(new { Message = exception.Message });
      }
    }
  }

}
