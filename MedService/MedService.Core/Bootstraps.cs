﻿using MedService.Core.Domains.Parkensons.Services;
using MedService.Core.Domains.ShortsNews.Services;
using MedService.Core.Domains.Users.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core
{
  public static class Bootstraps
  {
    public static IServiceCollection AddCore(this IServiceCollection services)
    {
      services.AddScoped<IShortNewsService, ShortNewsService>();
      services.AddScoped<IUserService, UserService>();
      services.AddScoped<IParkensonService, ParkensonService>();
      return services;
    }
  }
}
