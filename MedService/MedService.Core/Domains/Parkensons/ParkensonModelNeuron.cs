﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Parkensons
{
  public class ParkensonModelNeuron
  {
    public double[][] leftAtt { set; get; }
    public double[][] rightAtt { set; get; }
    public double[][] leftMot { set; get; }
    public double[][] rightMot { set; get; }
  }
}
