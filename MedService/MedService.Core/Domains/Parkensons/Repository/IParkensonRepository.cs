﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Parkensons.Repository
{
  public interface IParkensonRepository
  {
    public Task Create(string userId, double[][] left_att, double[][] right_att, double[][] left_mot, double[][] right_mot, string result);

    public Task Delete(string id);

    public Task<IEnumerable<ParkensonModel>> GetAll();

    public Task<IEnumerable<ParkensonModel>> GetByUserId(string userId);

    public Task<ParkensonModel> GetById(string Id);
  }
}
