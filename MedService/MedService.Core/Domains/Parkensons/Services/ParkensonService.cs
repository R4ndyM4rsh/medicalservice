﻿using MedService.Core.Domains.Parkensons.Repository;
using MedService.Core.Domains.Users.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Parkensons.Services
{
  public class ParkensonService : IParkensonService
  {

    private readonly IUserRepository _userRepository;
    private readonly IParkensonRepository _parkensonRepository;

    public ParkensonService(IUserRepository userRepository, IParkensonRepository parkensonRepository) 
    {
      _userRepository = userRepository;
      _parkensonRepository = parkensonRepository;
      
    }

    public async Task Create(string userId, double[][] left_att, double[][] right_att, double[][] left_mot, double[][] right_mot)
    {
      string result;
      if (await _userRepository.GetUserById(userId) == null) 
      {
        throw new ValidationException("Пользователь с таким id не существует");
      }
      if (CheckMatrixDimension(left_att) || CheckMatrixDimension(right_att) 
        || CheckMatrixDimension(left_mot) || CheckMatrixDimension(right_mot)) 
      {
        throw new ValidationException("Матрица не подходящей размерности");
      }
      if (CheckMatrixOnValidData(left_att) || CheckMatrixOnValidData(right_att) 
        || CheckMatrixOnValidData(left_mot) || CheckMatrixOnValidData(right_mot)) 
      {
        throw new ValidationException("Некоректные значения матриц");
      }
      
      HttpClient client = new HttpClient();

      client.BaseAddress = new Uri("http://127.0.0.1:5000/");
      client.DefaultRequestHeaders.Accept.Clear();
      client.DefaultRequestHeaders.Accept.Add(
          new MediaTypeWithQualityHeaderValue("application/json"));
      try
      {
        HttpResponseMessage response = await client.PostAsJsonAsync(
        "analyse", new ParkensonModelNeuron
        {
          leftAtt = left_att,
          rightAtt = right_att,
          leftMot = left_mot,
          rightMot = right_mot
        });
        result = await response.Content.ReadAsStringAsync();
      }
      catch (Exception) 
      {
        throw new Exception("Неудается подключится к серверу");
      }
      await _parkensonRepository.Create(userId, left_att, right_att, left_mot, right_mot, result);
    }

    public async Task Delete(string id)
    {
      if (await _parkensonRepository.GetById(id) == null) 
      {
        throw new Exception("Анализа с таким id не существует");
      }
      await _parkensonRepository.Delete(id);
    }

    public async Task<IEnumerable<ParkensonModel>> GetAll()
    {
      return await _parkensonRepository.GetAll();
    }

    public async Task<ParkensonModel> GetById(string Id)
    {
      return await _parkensonRepository.GetById(Id);
    }

    public async Task<IEnumerable<ParkensonModel>> GetByUserId(string userId)
    {
      if (await _parkensonRepository.GetByUserId(userId) == null) 
      {
        throw new Exception("Пользователя с таким id не существует");
      }
      return await _parkensonRepository.GetByUserId(userId);
    }

    private bool CheckMatrixOnValidData(double[][] matrix) 
    {
      for (int i = 0; i < 21; i ++) 
      {
        for (int j = 0; j < 21; j++) 
        {
          if (matrix[i][j] > 1 || matrix[i][j] < -1) return true;
        }
      }
      return false;
    }
    static void RunSettings()
    {
      
    }
    private bool CheckMatrixDimension(double[][] matrix) 
    {
         if (matrix.GetLength(0) != 21 || matrix.GetLength(1) != 21) return false;
         return true;
    }
  }
}
