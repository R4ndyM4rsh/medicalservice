﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Parkensons
{
  public class ParkensonModel
  {
    public string Id { set; get; }
    public string userId { set; get; }
    public double[][] leftAtt { set; get; }
    public double[][] rightAtt { set; get; }
    public double[][] leftMot { set; get; }
    public double[][] rightMot { set; get; }
    public string result { set; get; }
  }
}
