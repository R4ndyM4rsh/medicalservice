﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Users
{
  public class User
  {
    public string Id { set; get; }
    public string Login { set; get; }
    public byte[] Avatar { get; set; }
    public string Email { set; get; }
    public string Name { set; get; }

  }
}
