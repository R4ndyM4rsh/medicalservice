﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Users.Services
{
  public interface IUserService
  {
    public Task<User> GetUserById(string id);
    public Task<IEnumerable<User>> GetAll();
    public Task Create(string login, string email, string name, IFormFile avatar);
    public Task Update(string id, string login, string email, string name);
    public Task Delete(string id);
  }
}
