﻿using MedService.Core.Domains.Users.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Users.Services
{
  public class UserService : IUserService
  {

    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository) 
    {
      _userRepository = userRepository;
    }
    public async Task Create(string login, string email, string name, IFormFile avatar)
    {
      if (login == null) 
      {
        throw new ValidationException("Нельзя создать пользователя с пустым логином");
      }
      if (email == null) 
      {
        throw new ValidationException("Нельзя создать пользователя с пустой почтой");
      }
      if (name == null) 
      {
        throw new ValidationException("Нельзя создать пользователя с пустым именем");
      }
      if (avatar == null) 
      {
        throw new ValidationException("Нельзя создать пользователя без аватара");
      }
      byte[] imageData = null;
      using (var binaryReader = new BinaryReader(avatar.OpenReadStream()))
      {
        imageData = binaryReader.ReadBytes((int)avatar.Length);
      }
      await _userRepository.Create(login, email, name, imageData);
    }

    public async Task Delete(string id)
    {
      await _userRepository.Delete(id);
    }

    public async Task<IEnumerable<User>> GetAll()
    {
      return await _userRepository.GetAll();
    }

    public async Task<User> GetUserById(string id)
    {
      return await _userRepository.GetUserById(id);
    }

    public async Task Update(string id, string login, string email, string name)
    {
      await _userRepository.Update(id, login, email, name);
    }
  }
}
