﻿using MedService.Core.Domains.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Comments.Repository
{
  public interface ICommentRepository
  {
    public Task CreateComment(string newsId, string text, User user);

    public Task DeleteComment(string id);

    public Task UpdateComment(string id, string text);

    public Task<Comment> GetCommentById(string id);

    public Task<IEnumerable<Comment>> GetAllComments();
  }
}
