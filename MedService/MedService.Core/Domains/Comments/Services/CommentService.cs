﻿using MedService.Core.Domains.Comments.Repository;
using MedService.Core.Domains.ShortsNews.Repository;
using MedService.Core.Domains.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Comments.Services
{
  public class CommentService : ICommentService
  {

    private readonly ICommentRepository _commentRepository;
    private readonly IShortNewsRepository _shortNewsRepository;

    public CommentService(ICommentRepository commentRepository, IShortNewsRepository shortNewsRepository) 
    {
      _commentRepository = commentRepository;
      _shortNewsRepository = shortNewsRepository;
    }

    public async Task CreateComment(string newsId, string text, User user)
    {
      var news = await _shortNewsRepository.GetShortNewsById(newsId);
      if (news == null) 
      {
        throw new Exception("Такой новости не существует");
      }
      if (user == null) 
      {
        throw new Exception("Такого пользователя не существует");
      }
      if (text == null) 
      {
        throw new Exception("Нельзя добавить пустой комментарий");
      }
      await _commentRepository.CreateComment(newsId, text, user);
    }

    public async Task DeleteComment(string id)
    {
      await _commentRepository.DeleteComment(id);
    }

    public async Task<IEnumerable<Comment>> GetAllComments()
    {
      return await _commentRepository.GetAllComments();
    }

    public async Task<Comment> GetCommentById(string id)
    {
      return await _commentRepository.GetCommentById(id);
    }

    public async Task UpdateComment(string id, string text)
    {
      if (text == null)
      {
        throw new Exception("Нельзя изменить комментарий на пустой");
      }
      await _commentRepository.UpdateComment(id, text);
    }
  }
}
