﻿using MedService.Core.Domains.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.Comments
{
  public class Comment
  {
    public string Id { set; get; }

    public string ShortNewsId { set; get; }

    public User User { set; get; }

    public string Text { set; get; }

    public bool isUpdate { set; get; }

    public DateTime TimeCreate { set; get; }
  }
}
