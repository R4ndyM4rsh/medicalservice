﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using MedService.Core.Domains.Comments;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.ShortsNews
{
  public class ShortNews
  {
    public string Id { set; get; }
    public string Header { set; get; }
    public string Body { set; get; }
    public DateTime CreateDate { set; get; }

    public IEnumerable<Comments.Comment> Comments { set; get; }
  }
}
