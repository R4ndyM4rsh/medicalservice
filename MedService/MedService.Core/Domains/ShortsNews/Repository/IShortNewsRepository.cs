﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.ShortsNews.Repository
{
  public interface IShortNewsRepository
  {
    public Task Update(string id, string header, string body);

    public Task CreateShortNews(string header, string body);

    public Task<ShortNews> GetShortNewsById(string id);

    public Task<IEnumerable<ShortNews>> GetAllShortsNews();

    public Task Delete(string id);
  }
}
