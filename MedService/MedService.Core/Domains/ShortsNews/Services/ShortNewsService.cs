﻿using MedService.Core.Domains.ShortsNews.Repository;
using MedService.Core.Domains.Users.Repository;
using MedService.Core.Domains.ShortsNews;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Core.Domains.ShortsNews.Services
{
  public class ShortNewsService : IShortNewsService
  {
    private readonly IShortNewsRepository _shortNewsRepository;

    public ShortNewsService(IShortNewsRepository shortNewsRepository) 
    {
      _shortNewsRepository = shortNewsRepository;
    }
    public async Task CreateShortNews(string header, string body)
    {
      if (header == null) 
      {
        throw new Exception("Заголовок отсутствует");
      }
      if (body == null) 
      {
        throw new Exception("Текст отсутствует");
      }
      await _shortNewsRepository.CreateShortNews(header, body);
    }

    public async Task<IEnumerable<ShortNews>> GetAllShortsNews()
    {
      return await _shortNewsRepository.GetAllShortsNews();
    }

    public async Task<ShortNews> GetShortNewsById(string id)
    {
      if (id == null) 
      {
        throw new Exception("Отсутствует Id");
      }
      return await _shortNewsRepository.GetShortNewsById(id);
    }

    public async Task Update(string id, string header, string body) 
    {
      if (header == null)
      {
        throw new Exception("Заголовок отсутствует");
      }
      if (body == null)
      {
        throw new Exception("Текст отсутствует");
      }
      await _shortNewsRepository.Update(id, header, body);
    }

    public async Task Delete(string id) 
    {
      var shortNews = await _shortNewsRepository.GetShortNewsById(id);
      if (shortNews == null) 
      {
        throw new Exception("Такого аккаунта не существует");
      }
      await _shortNewsRepository.Delete(id);
    }
  }
}
