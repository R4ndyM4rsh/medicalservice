﻿using MedService.Data.Comments;
using MedService.Data.Parkensons;
using MedService.Data.Snapshots;
using MedService.Data.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data
{
  public class MedServiceContext : DbContext
  {
    public DbSet<UserDbModel> Users { set; get; }
    public DbSet<ShortNewsDbModel> ShortNews { set; get; }
    public DbSet<CommentDbModel> Comment { set; get; }
    public DbSet<ParkensonDbModel> Parkenson { set; get; }

    public MedServiceContext(DbContextOptions options) : base(options)
    {
    
    }

    public class Factory : IDesignTimeDbContextFactory<MedServiceContext>
    {
      public MedServiceContext CreateDbContext(string[] args)
      {
        var options = new DbContextOptionsBuilder()
          .UseNpgsql("Host=localhost;Port=5432;Database=medservice;Username=postgres;Password=12345")
          .Options;
        return new MedServiceContext(options);
      }
    }

  }
}
