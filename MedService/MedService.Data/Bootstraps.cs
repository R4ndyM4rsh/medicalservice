﻿using MedService.Core.Domains.Parkensons.Repository;
using MedService.Core.Domains.ShortsNews.Repository;
using MedService.Core.Domains.Users.Repository;
using MedService.Data.Parkensons.Repositories;
using MedService.Data.Snapshots.Repositories;
using MedService.Data.Users.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data
{
  public static class Bootstraps
  {
    public static IServiceCollection AddData(this IServiceCollection services)
    {
      services.AddScoped<IShortNewsRepository, ShortNewsRepository>();
      services.AddScoped<IUserRepository, UserRepository>();
      services.AddScoped<IParkensonRepository, ParkensonRepository>();
      services.AddDbContext<MedServiceContext>();
      return services;
    }
  }
}
