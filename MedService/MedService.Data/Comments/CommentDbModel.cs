﻿using MedService.Data.Snapshots;
using MedService.Data.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Comments
{
  public class CommentDbModel
  {
    public string Id { set; get; }

    public string ShortNewsId { set; get; }

    public string Text { set; get; }

    public bool isUpdate { set; get; }
    public DateTime TimeCreate { set; get; }

    public UserDbModel User { set; get; }
    public ShortNewsDbModel ShortNews { set; get; }

    internal class Map : IEntityTypeConfiguration<CommentDbModel>
    {
      public void Configure(EntityTypeBuilder<CommentDbModel> builder)
      {
        builder.ToTable("comments");
        builder.Property(it => it.Id)
          .HasColumnName("id");
        builder.Property(it => it.User)
          .HasColumnName("user");
        builder.Property(it => it.Text)
          .HasColumnName("text");
        builder.Property(it => it.TimeCreate)
          .HasColumnName("time_create");
        builder.Property(it => it.isUpdate)
          .HasColumnName("is_update");

        builder.HasOne(it => it.ShortNews)
         .WithMany(it => it.Comments)
         .HasForeignKey(it => it.ShortNewsId);

      }
    }
  }
}
