﻿using MedService.Core.Domains.Comments;
using MedService.Core.Domains.Comments.Repository;
using MedService.Core.Domains.Users;
using MedService.Data.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Comments.Repositories
{
  public class CommentRepository : ICommentRepository
  {
    private readonly MedServiceContext _context;

    public CommentRepository(MedServiceContext context) 
    {
      _context = context;
    }
    public async Task CreateComment(string newsId, string text, User user)
    {
      var entity = new CommentDbModel
      {
        Id = Guid.NewGuid().ToString(),
        ShortNewsId = newsId,
        Text = text,
        User = new UserDbModel 
        {
          Id = user.Id,
          Login = user.Login,
          Email = user.Email,
          Name = user.Name,
          Avatar = user.Avatar
        },
        isUpdate = false,
        TimeCreate = DateTime.UtcNow
      };
      await _context.Comment.AddAsync(entity);
      _context.SaveChanges();
    }

    public async Task DeleteComment(string id)
    {
      var entity = await _context.Comment.FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null) 
      {
        throw new Exception("такого комментария нет");
      };
      _context.Comment.Remove(entity);
    }

    public async Task<IEnumerable<Comment>> GetAllComments()
    {
      return await _context.Comment
        .AsNoTracking()
        .Select(it => new Comment()
        {
          Id = it.Id,
          ShortNewsId = it.ShortNewsId,
          Text = it.Text,
          User = new User 
          {
            Id = it.User.Id,
            Login = it.User.Login,
            Avatar = it.User.Avatar,
            Email = it.User.Email,
            Name = it.User.Name
          },
          TimeCreate = it.TimeCreate,
          isUpdate = it.isUpdate
        }).ToListAsync();
    }

    public async Task<Comment> GetCommentById(string id)
    {
      var entity = await _context.Comment
        .AsNoTracking()
        .FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null)
      {
        return null;
      }
      return new Comment
      {
        Id = entity.Id,
        Text = entity.Text,
        ShortNewsId = entity.ShortNewsId,
        isUpdate = entity.isUpdate,
        TimeCreate = entity.TimeCreate,
        User = new User 
        {
          Id = entity.User.Id,
          Login = entity.User.Login,
          Avatar = entity.User.Avatar,
          Email = entity.User.Email,
          Name = entity.User.Name
        }
      };

    }

    public async Task UpdateComment(string id, string text)
    {
      var entity = await _context.Comment.FirstOrDefaultAsync(it => it.Id == id);
      entity.Text = text;
      _context.SaveChanges();
    }
  }
}
