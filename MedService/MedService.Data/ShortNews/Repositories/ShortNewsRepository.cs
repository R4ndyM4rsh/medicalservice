﻿using MedService.Core.Domains;
using MedService.Core.Domains.ShortsNews.Repository;
using MedService.Core.Domains.Users;
using MedService.Core.Domains.ShortsNews;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedService.Core.Domains.Comments;

namespace MedService.Data.Snapshots.Repositories
{
  public class ShortNewsRepository : IShortNewsRepository
  {
    private readonly MedServiceContext _context;

    public ShortNewsRepository(MedServiceContext context)
    {
      _context = context;
    }

    public async Task CreateShortNews(string header, string body)
    {
      var entity = new ShortNewsDbModel
      {
        Id = Guid.NewGuid().ToString(),
        Header = header,
        Body = body,
        CreateDate = DateTime.UtcNow
      };
      await _context.ShortNews.AddAsync(entity);
      _context.SaveChanges();
    }

    public async Task<IEnumerable<ShortNews>> GetAllShortsNews()
    {
      return await _context.ShortNews
        .AsNoTracking()
        .Select(it => new ShortNews()
        {
          Id = it.Id,
          Header = it.Header,
          Body = it.Body,
          CreateDate = it.CreateDate,
          Comments = it.Comments.Select(co => new Comment
          {
            Id = co.Id,
            ShortNewsId = co.Id,
            Text = co.Text,
            TimeCreate = co.TimeCreate,
            isUpdate = co.isUpdate,
            User = new User
            {
              Id = co.User.Id,
              Login = co.User.Login,
              Avatar = co.User.Avatar,
              Email = co.User.Email,
              Name = co.User.Name
            }
          })
        }).ToListAsync();
    }

    public async Task<ShortNews> GetShortNewsById(string id)
    {
      var entity = await _context.ShortNews
        .AsNoTracking()
        .FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null) 
      {
        return null;
      }
      return new ShortNews
      {
        Id = id,
        Header = entity.Header,
        Body = entity.Body,
        CreateDate = entity.CreateDate,
        Comments = entity.Comments.Select(co => new Comment
        {
          Id = co.Id,
          ShortNewsId = co.Id,
          Text = co.Text,
          TimeCreate = co.TimeCreate,
          isUpdate = co.isUpdate,
          User = new User
          {
            Id = co.User.Id,
            Login = co.User.Login,
            Avatar = co.User.Avatar,
            Email = co.User.Email,
            Name = co.User.Name
          }
        })
      };
    }

    public async Task Update(string id, string header, string body)
    {
      var entity = await _context.ShortNews.FirstOrDefaultAsync(it => it.Id == id);
      entity.Header = header;
      entity.Body = body;
      _context.SaveChanges();
    }

    public async Task Delete(string id) 
    {
      var entity = await _context.ShortNews.FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null) 
      {
        throw new Exception("Такой новости не существует");
      }
      _context.ShortNews.Remove(entity);
    }
  }
}
