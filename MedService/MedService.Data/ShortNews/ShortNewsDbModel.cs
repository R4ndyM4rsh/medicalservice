﻿using MedService.Data.Comments;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Snapshots
{
  public class ShortNewsDbModel
  {
    public string Id { set; get; }
    public string Header { set; get; }
    public string Body { set; get; }
    public DateTime CreateDate { set; get; }
    public IEnumerable<CommentDbModel> Comments { set; get; }

    internal class Map : IEntityTypeConfiguration<ShortNewsDbModel>
    {
      public void Configure(EntityTypeBuilder<ShortNewsDbModel> builder)
      {
        builder.ToTable("short_news");
        builder.Property(it => it.Id)
          .HasColumnName("id");
        builder.Property(it => it.Header)
          .HasColumnName("header");
        builder.Property(it => it.Body)
          .HasColumnName("body");
        builder.Property(it => it.CreateDate)
          .HasColumnName("create_date");
      }
    }
  }
}
