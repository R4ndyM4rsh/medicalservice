﻿using MedService.Data.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Parkensons
{
  public class ParkensonDbModel
  {
    public string Id { set; get; }

    public string UserId { set; get; }

    public string[] AnalysisArray { set; get; }

    public string Result { set; get; }

    public UserDbModel User { set; get; }
    internal class Map : IEntityTypeConfiguration<ParkensonDbModel>
    {
      public void Configure(EntityTypeBuilder<ParkensonDbModel> builder)
      {
        builder.ToTable("parkensons");
        builder.Property(it => it.Id)
          .HasColumnName("id");
        builder.Property(it => it.UserId)
          .HasColumnName("user_id");
        builder.Property(it => it.AnalysisArray)
          .HasColumnName("analysis_array");
        builder.Property(it => it.Result)
          .HasColumnName("result");

        builder.HasOne(it => it.User)
         .WithMany(it => it.Parkensons)
         .HasForeignKey(it => it.UserId);
      }
    }
  }
}
