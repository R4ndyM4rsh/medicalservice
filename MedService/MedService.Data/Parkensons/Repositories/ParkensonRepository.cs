﻿using MedService.Core.Domains.Parkensons;
using MedService.Core.Domains.Parkensons.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Parkensons.Repositories
{
  public class ParkensonRepository : IParkensonRepository
  {
    private readonly MedServiceContext _context;

    public ParkensonRepository(MedServiceContext context)
    {
      _context = context;
    }

    public async Task Create(string userId, double[][] leftAtt, double[][] rightAtt, double[][] leftMot, double[][] rightMot, string result)
    {
      string[] matrix = new string[4];

      matrix[0] = ArrayConversionToString(leftAtt);
      matrix[1] = ArrayConversionToString(rightAtt);
      matrix[2] = ArrayConversionToString(leftMot);
      matrix[3] = ArrayConversionToString(rightMot);

      var entity = new ParkensonDbModel()
      {
        Id = Guid.NewGuid().ToString(),
        UserId = userId,
        AnalysisArray = matrix,
        Result = result
      };
      await _context.Parkenson.AddAsync(entity);
      _context.SaveChanges();
    }

    public async Task Delete(string id)
    {

      var entity = await _context.Parkenson.FirstOrDefaultAsync(it => it.Id == id);

      if (entity == null)
      {
        throw new Exception("Такого анализа нет");
      };

      _context.Parkenson.Remove(entity);
    }

    public async Task<IEnumerable<ParkensonModel>> GetAll()
    {
      var result = await _context.Parkenson
        .AsNoTracking()
        .Select(it => new ParkensonModel()
        {
          Id = it.Id,
          userId = it.UserId,
          leftAtt = ParceStringToDoubleArr(it.AnalysisArray[0]),
          rightAtt = ParceStringToDoubleArr(it.AnalysisArray[1]),
          leftMot = ParceStringToDoubleArr(it.AnalysisArray[2]),
          rightMot = ParceStringToDoubleArr(it.AnalysisArray[3]),
          result = it.Result
        }).ToListAsync();
      return result;
    }

    public async Task<ParkensonModel> GetById(string Id)
    {
      var entity = await _context.Parkenson.FirstOrDefaultAsync(it => it.Id == Id);

      if (entity == null)
      {
        throw new Exception("такого комментария нет");
      };

      return new ParkensonModel
      {
        Id = entity.Id,
        userId = entity.UserId,
        leftAtt = ParceStringToDoubleArr(entity.AnalysisArray[0]),
        rightAtt = ParceStringToDoubleArr(entity.AnalysisArray[1]),
        leftMot = ParceStringToDoubleArr(entity.AnalysisArray[2]),
        rightMot = ParceStringToDoubleArr(entity.AnalysisArray[3]),
        result = entity.Result
      };
    }

    public async Task<IEnumerable<ParkensonModel>> GetByUserId(string userId)
    {
      return await _context.Parkenson
       .AsNoTracking()
       .Where(it => it.UserId == userId)
       .Select(it => new ParkensonModel
       {
         Id = it.Id,
         userId = it.UserId,
         leftAtt = ParceStringToDoubleArr(it.AnalysisArray[0]),
         rightAtt = ParceStringToDoubleArr(it.AnalysisArray[1]),
         leftMot = ParceStringToDoubleArr(it.AnalysisArray[2]),
         rightMot = ParceStringToDoubleArr(it.AnalysisArray[3]),
         result = it.Result
       }).ToListAsync();
    }

    private static double[][] ParceStringToDoubleArr(string str) 
    {
      double[][] arr = new double[21][];
      str = str.Remove(str.Length - 1);
      var lines = str.Split('!');

      for (int i = 0; i < 21; i++) 
      {
        var elem = lines[i].Remove(lines[i].Length - 1);
        var elems = elem.Split('?');
        arr[i] = new double[21];
        for (int j = 0; j < 21; j++) 
        {
          arr[i][j] = Convert.ToDouble(elems[j]);
        }
      }
      return arr;
    }
    private string ArrayConversionToString(double[][] matrix)
    {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < 21; i++)
      {
        for (int j = 0; j < 21; j++)
        {
          var str = matrix[i][j].ToString();
          sb.Append(str);
          sb.Append('?');
        }
        sb.Append('!');
      }
      return sb.ToString();
    }
  }
}
