﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MedService.Data.Migrations
{
    public partial class change_parkenson_model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LeftAtt",
                table: "Parkenson");

            migrationBuilder.DropColumn(
                name: "LeftMot",
                table: "Parkenson");

            migrationBuilder.DropColumn(
                name: "RightAtt",
                table: "Parkenson");

            migrationBuilder.DropColumn(
                name: "RightMot",
                table: "Parkenson");

            migrationBuilder.AddColumn<string[]>(
                name: "AnalysisArray",
                table: "Parkenson",
                type: "text[]",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnalysisArray",
                table: "Parkenson");

            migrationBuilder.AddColumn<double[]>(
                name: "LeftAtt",
                table: "Parkenson",
                type: "double precision[]",
                nullable: true);

            migrationBuilder.AddColumn<double[]>(
                name: "LeftMot",
                table: "Parkenson",
                type: "double precision[]",
                nullable: true);

            migrationBuilder.AddColumn<double[]>(
                name: "RightAtt",
                table: "Parkenson",
                type: "double precision[]",
                nullable: true);

            migrationBuilder.AddColumn<double[]>(
                name: "RightMot",
                table: "Parkenson",
                type: "double precision[]",
                nullable: true);
        }
    }
}
