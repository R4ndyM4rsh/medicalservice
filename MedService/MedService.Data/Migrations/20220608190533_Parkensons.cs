﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MedService.Data.Migrations
{
    public partial class Parkensons : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isUpdate",
                table: "Comment",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Parkenson",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    LeftAtt = table.Column<double[,]>(type: "double precision[]", nullable: true),
                    RightAtt = table.Column<double[,]>(type: "double precision[]", nullable: true),
                    LeftMot = table.Column<double[,]>(type: "double precision[]", nullable: true),
                    RightMot = table.Column<double[,]>(type: "double precision[]", nullable: true),
                    Result = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parkenson", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Parkenson_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Parkenson_UserId",
                table: "Parkenson",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Parkenson");

            migrationBuilder.DropColumn(
                name: "isUpdate",
                table: "Comment");
        }
    }
}
