﻿using MedService.Data.Parkensons;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Users
{
  public class UserDbModel
  {
    public string Id { set; get; }
    public string Login { set; get; }
    public byte[] Avatar { get; set; }
    public string Email { set; get; }
    public string Name { set; get; }

    public IEnumerable<ParkensonDbModel> Parkensons { set; get; }

    internal class Map : IEntityTypeConfiguration<UserDbModel>
    {
      public void Configure(EntityTypeBuilder<UserDbModel> builder)
      {
        builder.ToTable("user");
        builder.Property(it => it.Id)
          .HasColumnName("id");
        builder.Property(it => it.Login)
          .HasColumnName("login");
        builder.Property(it => it.Avatar)
          .HasColumnName("avatar");
        builder.Property(it => it.Email)
          .HasColumnName("email");
        builder.Property(it => it.Name)
          .HasColumnName("name");
      }
    }

  }
}
