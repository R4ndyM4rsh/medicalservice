﻿using MedService.Core.Domains.Users;
using MedService.Core.Domains.Users.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedService.Data.Users.Repositories
{
  public class UserRepository : IUserRepository
  {
    private readonly MedServiceContext _context;

    public UserRepository(MedServiceContext context) 
    {
      _context = context;
    }

    public async Task Create(string login, string email, string name, byte[] avatar)
    {
      
      var entity = new UserDbModel
      {
        Id = Guid.NewGuid().ToString(),
        Login = login,
        Email = email,
        Name = name,
        Avatar = avatar
      };
      await _context.Users.AddAsync(entity);
      _context.SaveChanges();
    }

    public async Task Delete(string id)
    {
      var entity = await _context.Users.FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null) 
      {
        throw new Exception("Пользователя с данным id не существует");
      }
      _context.Users.Remove(entity);
      _context.SaveChanges();
    }

    public async Task<IEnumerable<User>> GetAll()
    {
      return await _context.Users
        .AsNoTracking()
        .Select(it => new User()
      {
        Id = it.Id,
        Login = it.Login,
        Email = it.Email,
        Name = it.Name,
        Avatar = it.Avatar
      }).ToListAsync();
    }

    public async Task<User> GetUserById(string id)
    {
      var entity = await _context.Users
        .AsNoTracking()
        .FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null) 
      {
        return null;
      }
      return new User
      {
        Id = entity.Id,
        Login = entity.Login,
        Email = entity.Email,
        Name = entity.Name,
        Avatar = entity.Avatar
      };
    }

    public async Task Update(string id, string login, string email, string name)
    {
      var entity = await _context.Users.FirstOrDefaultAsync(it => it.Id == id);
      if (entity == null)
      {
        throw new Exception("Пользователя с данным id не существует");
      }
      entity.Login = login;
      entity.Email = email;
      entity.Name = name;
      _context.SaveChanges();
    }
  }
}
